use nannou::prelude::*;
use nannou::app::Draw;
// use nannou::math::cgmath::{self, Point2};

struct DrawChain {
    resolution: Point2,
    passes: Vec<Box<DrawPass>>,
    time: f64,
}


impl DrawChain {
    fn new(resolution: Point2) -> DrawChain {
        let passes: Vec<Box<DrawPass>> = Vec::new();
        let time = 0.0;
        DrawChain {
            resolution,
            passes,
            time,
        }
    }
    fn add_pass(&mut self, pass: Box<DrawPass>) {
        self.passes.push(pass);
    }
}


trait DrawPass {
    fn do_draw(&self, draw: &Draw);
}


struct BasicDrawPass {
    red_value: f32,
}

impl DrawPass for BasicDrawPass {
    fn do_draw(&self, draw: &Draw){
        draw.rect()
               .x_y(0., 0.)
               .w(420.)
               .color(Rgb::new(self.red_value, 0.0, 0.0));

    }
}

struct CircleDrawPass {
    size:f32,
}

impl DrawPass for CircleDrawPass {
    fn do_draw(&self, draw: &Draw) {
        draw.ellipse()
               .x_y(0., 0.)
               .radius(self.size)
               .color(RED);
    }
}

fn main() {
    nannou::app(model)
        .update(update)
        .simple_window(view)
        .run();
}

struct Model {
    draw_chain: DrawChain,
}

fn model(_app: &App) -> Model {
    let res = _app.window_rect().bottom_right();
    let mut draw_chain = DrawChain::new(res);
    let red_pass = BasicDrawPass{red_value: 0.3};

    draw_chain.add_pass(Box::new(red_pass));
    draw_chain.add_pass(Box::new(CircleDrawPass{size:10.0}));


    Model {
        draw_chain,
    }
}

fn update(_app: &App, _model: &mut Model, _update: Update) {
}

fn view(_app: &App, _model: &Model, frame: Frame){
    let draw = _app.draw();
    draw.background().color(CORNFLOWERBLUE);
    // let a_draw_pass = BasicDrawPass{red_value: 0.5};
    // a_draw_pass.do_draw(&draw);
    // let b_draw_pass = CircleDrawPass{size: 100.0};
    // b_draw_pass.do_draw(&draw);

    // for p in _model.draw_chain.passes.iter() {
    //     p.do_draw(frame);
    // }
    _model.draw_chain.passes
        .iter()
        .for_each(|p| p.do_draw(&draw));
    frame.clear(PURPLE);
    draw.to_frame(_app, &frame).unwrap();
}
